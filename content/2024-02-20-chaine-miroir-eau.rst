[20/02/2024] [Bordeaux | Miroir d'Eau] Chaîne Humaine
################################################################################

:date: 2024-02-20 18:00
:category: Rassemblements

POINT DE RENCONTRE, au MIROIR d'EAU le 20 FEVRIER à 18h

C'est donc la chaîne humaine évoquée pour le jour J, dont il est question
dans les flyers qu'il a été distribué.

Des vidéos prises lors des prises de parole ont été publiées sur
la chaîne peertube de Free Assange Wage:

https://tube.aquilenet.fr/c/free_assange_wave/videos?s=1
