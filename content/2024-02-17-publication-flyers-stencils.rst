[17/02/2024] Publication des .pdf de Flyers Stencils pour le 20-21 Février
################################################################################

:date: 2024-02-17 21:00
:category: Flyers-Affiches-etc


Publication des .pdf des Flyers composé à partir d'un Stencil

Annoncée sur les listes de diffusion
bistro@aquilenet.fr ,
blahblah@listes.abul.org ,
organisation@listes.giroll.org ,
mailing@labx.fr ,
labx-2@framagroupes.org .


Les fichiers sont disponibles au téléchargement à l'adresse suivante:

    https://envs.sh/Fkg.zip 

Ils ont été combinés aux fichiers imprimables initialement
publiés par Aquilenet disponibles ici:

    https://cloud.aquilenet.fr/s/PH9PoR5s5BxFrTt

Les mêmes stencils, sans les dates 20-21 février,
seront publiés plus tard à l'adresse suivante:

        https://envs.sh/Fr0.zip 
