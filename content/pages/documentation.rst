Documentation
################################################################################

:page_order: 5300


.. note:: 
    **Attention!** : *On ne va pas pouvoir tenir à
    jour ici une liste exhaustive de ressources,
    ce serait fastidieux.
    Et ces ressources restent les mêmes quelque soit la ville.
    Nous nous contenteront donc de rassembler
    des liens vers quelques sites utiles,
    qui fournissent déjà une liste exhaustive
    et à jour.*

.. contents::  .
 :depth: 5
 
.. sectnum::


------------------------------------------------------------------------


Documentation Francophone Locale
================================================================================

Vidéo Peertube d'Aquilenet
---------------------------

La Chaîne Peertube de Free Assange Wave
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Free Assange Wave Bordeaux
a mis en ligne
plusieurs vidéos
accessibles en streaming,
dont certaines ont été prises à Bordeaux.

    https://tube.aquilenet.fr/c/free_assange_wave/videos?s=1

D'autres Vidéos à propos d'Assange
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

D'autres vidéos que celles
de la chaîne de Free Assange Wave Bordeaux,
au sujet d'Assange,
sont aussi visionnable en streaming
depuis le Peertube d'Aquilenet.
En utilisant la fonction recherched de Peertube:

    https://tube.aquilenet.fr/search?search=Assange&searchTarget=local



Documentation Francophone
================================================================================

Peertube tube.nogafa.org
------------------------------------------------------------------------
Une chaîne de vidéos Peertube,
avec des vidéos en français,
et surtout les vidéos des diverses prises
de paroles à Londres au moment des procès:

    https://tube.nogafa.org/search?search=Assange&searchTarget=local

Avec par exemple ici celles de Stella Morris
et du père de Julian Assange
à la sortie de la séance du 20 mai:

    https://tube.nogafa.org/w/meQzz7atdcq87ySE5kkTFj


Comité Assange
------------------------------------------------------------------------
Le site web du Comité Assange
rassemble des liens vers l'essentiel des 
références utiles.
Notamment en bas de la page,
vers les articles publiés dans la presse,
des journaux qui accompagne
la mobilisation.

    https://comiteassange.fr/ressources/

En plus des livres et articles,
i y a aussi des ressources Vidéos
et
des Émissions de Radio.


liberonsassange.fr
------------------------------------------------------------------------
Un site maintenu par des français à Londres,
qui rassemble en ligne
une incroyable quantité d'articles
et autres documents traduits:

    https://liberonsassange.fr/


challengepower.info/fr
------------------------------------------------------------------------
C'est un Wiki qui est moins maintenu aujourd'hui,
mais qui rassemble aussi beaucoup d'informations en français.

    https://challengepower.info/fr/start

Il a aussi une section générale anglophone,
elle aussi peu alimentée actuellement,
mais assez exhaustive.

    https://challengepower.info/

vidéos: video.emergeheart.info
------------------------------------------------------------------------

C'est une Chaîne Peertube,
qui publie des vidéos accessibles gratuitement
en streaming:

    https://video.emergeheart.info/videos/trending?s=1

Ce sont surtout des vidéos en anglais,
mais il y a aussi des vidéos en français.
`Stella Morris à la fête de l'Humanité
<https://video.emergeheart.info/w/fMysW32gtkWbjLN7LxVeNt>`_
,
`Interview de Nils Meyer en français
<https://video.emergeheart.info/w/k6AdhE1TEj8uarX95pzhAT>`_
,
Le documentaire LCP
`Assange: le Prix de la Vérité
<https://video.emergeheart.info/w/fjNwfQhAQ3nG6g55Zo2m5e>`_
,
`Deux Ans à Belmarsh
<https://video.emergeheart.info/w/mE5g2XbERhoZpa1pPF7VSr>`_
,
*etc...*

Documentation Internationale
================================================================================

challengepower.info
------------------------------------------------------------------------
C'est le Wiki qui a déjà été évoqué
dans la documentation
francophone

    https://challengepower.info/

video.emergeheart.info
------------------------------------------------------------------------
La Chaîne de Streaming Peertube
qui a elle aussi été évoquée plus haut

    https://video.emergeheart.info/videos/trending?s=1

worldtomorrow.wikileaks.org
------------------------------------------------------------------------

Une série d'interview qui avaient été réalisées
par Assange, qui a rencontré et échangé avec
des personnalités diverses

    https://worldtomorrow.wikileaks.org/

Chaque épisode a une page propre,
qui propose des liens vers des traductions
de ces interviews en diverses langues.

Les épisodes 8 et 9 sont à propos du mouvement Cypherpunk.
