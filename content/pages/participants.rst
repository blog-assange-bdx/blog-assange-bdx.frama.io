Participants
################################################################################

:page_order: 5000


.. contents::  .
 :depth: 2

------------------------------------------------------------------------------


Participants à la Campagne
================================================================================

Websites
------------------------------------------------------------------------------
Free Assange Wage Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
https://defcon.social/@freeassangewavebordeaux



Amnesty International Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
https://www.amnesty.fr/pres-de-chez-vous/bordeaux-sud

https://www.amnesty.fr/militants-julian-assange-mobilisation-pour-sa-liberation



Ligue des Droits de l'Homme Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
https://www.facebook.com/LDHBordeauxGironde/



Aquilenet
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
https://www.aquilenet.fr/actualit%C3%A9s/libert%C3%A9-pour-julian-assange



Partenaires de la Campagne
================================================================================

Ce sont des organisations
qui ont participé
à la mise en place
d'évènements
pendant la campagne.

Théatre du Levain
------------------------------------------------------------------------------
https://theatre-levain.fr/au-programme/

https://www.facebook.com/theatre.le.levain/?locale=fr_FR



Cinéma Utopia
------------------------------------------------------------------------------
http://www.cinemas-utopia.org/bordeaux/



Cinéma Carbon Blanc
------------------------------------------------------------------------------
https://www.carbon-blanc.fr/mes-loisirs-associations/cinema.html

https://artec-cinemas.com/nos-salles/



Amis du Monde Diplomatique Gironde
------------------------------------------------------------------------------
https://www.amis.monde-diplomatique.fr/-Gironde-.html

.. _orgas-nationales:

Organisations Nationales
================================================================================
Les organisations précédentes étaient en Gironde.
Il peut être intéressant de se tenir informé de
l'actualité de diverses organisations
qui ont un cadre national

Comité Assange
------------------------------------------------------------------------------

Le Comité de Défense d'Assange pour la France.
Il n'y a pas qu'un compte Twitter.
https://twitter.com/ComiteAssange
Ils ont aussi un site web.
https://comiteassange.fr/ .

Et une lettre d'information.
Il n'y a pas de page pour l'abonnement,
mais il est possible de les contacter
en utilisant l'adresse email
qui est donnée sur leur site.
Et sur leur page twitter.

Ils ont sur leur site une page
https://comiteassange.fr/evenements/
qui permet de relayer les évènements
qu'ils organisent
dans le cadre de cette campagne.


Producteur de Cinéma: www.lesmutins.org
------------------------------------------------------------------------------
C'est la Société de Production des films
Ithaka
et 
Hacking Justice.
Sur leur site ils ont un Agenda,
qui montre qu'il est encore possible
d'organiser des projections
de ces deux
films.
https://www.lesmutins.org/agenda
