Stickers-Flyers-etc
################################################################################

:page_order: 3000


.. |vign-affiche| image:: ../images/vignettes/vignette-affiche-wikileaks-qrcode.png
                         :target: ../images/qr-code-blog-assange/assange-affiche-qrcode.zip

.. |vign-affichette| image:: ../images/vignettes/vignette-affiche-wikileaks-qrcode-x4.png
                         :target: ../images/qr-code-blog-assange/assange-affichettes.zip


.. defs flyers


.. |vign-flyer-recto| image:: ../images/vignettes/vignette-assange-wikileaks-extradition-A4-x04.png
                         :target: ../images/qr-code-blog-assange/flyers-blog-assange.zip

.. |vign-flyer-verso| image:: ../images/vignettes/vignette-flyer-QRcode-blog-IPNS-A4x4.png
                         :target: ../images/qr-code-blog-assange/flyers-blog-assange.zip


.. |vign-flyer-x09-recto| image:: ../images/vignettes/vignette-assange-wikileaks-extradition-A4-x09.png
                         :target: ../images/qr-code-blog-assange/flyers-blog-assange.zip

.. |vign-flyer-x09-verso| image:: ../images/vignettes/vignette-flyer-QRcode-blog-IPNS-A4x9.png
                         :target: ../images/qr-code-blog-assange/flyers-blog-assange.zip

.. defs qr-codes

.. |vign-qrcode-2x3| image:: ../images/vignettes/vignette-qr-codes-a-decouper-2x3.png
                         :target: ../images/qr-code-blog-assange/qr-codes-a-decouper.zip

.. |vign-qrcode-3x4| image:: ../images/vignettes/vignette-qr-codes-a-decouper-3x4.png
                         :target: ../images/qr-code-blog-assange/qr-codes-a-decouper.zip



|vign-affiche|
|vign-affichette|
|vign-flyer-recto|
|vign-flyer-verso|
|vign-flyer-x09-recto|
|vign-flyer-x09-verso|
|vign-qrcode-2x3|

Ces affiches et flyers sont plutôt bien acceptés dans les bars,
les restaus, théatres, cinémas, pharmacies, commerces,
sandwicheries, salle d'attentes, etc...

Des flyers peuvent aussi être oubliés
aux caisses des supermarchés ou ailleurs

.. contents::  .
 :depth: 3

.. sectnum::

------------------------------------------------------------------------------


Stickers
================================================================================

Pour l'instant nous n'avons pas encore connaissance
d'un endroit à Bordeaux ou en Gironde
où il serait possible de se procurer
des stickers pour cette campagne.
(Mise à Jour dès que possible)

En revanche, en attendant, il est possible de trouver
des imprimeurs qui peuvent faire de faibles tirages.
Par exemple, nous avons trouvé
`versionprint.fr <https://www.versionprint.fr/>`_
au 36 Quai de Bacalan,
qui imprime des stickers sur page A4.
En N&B ou en couleur.
À partir d'une seule page A4.
La découpe est par contre à faire soi-même au cutter,
à cause de la colle qui encrasserait son massicot.

Il existe sûrement une foule d'autres
imprimeurs/ateliers de reprographie
en Gironde
qui peuvent proposer de faire la même chose.

Ce n'est évidement pas la même robustesse
qu'une impression de sticker industrielle.

Affiches
================================================================================

Le lien pour télécharger le fichier imprimable d'une affiche
avec le QR-code, d'une affiche sans QR code, et les fichiers
de brouillon pour pouvoir composer d'autres affiches en changeant
le QR code ou en rajoutant des éléments.

    https://envs.sh/tdK.zip 

|vign-affiche|

Téléchargement depuis ce site: `assange-affiche-qrcode.zip <../images/qr-code-blog-assange/assange-affiche-qrcode.zip>`_

Affichettes
================================================================================

Des affichettes imprimables,
avec le QRcode renvoyant vers le site,
qui prendront moins de place que du A4.
Sur un panneau d'affichage dela Fac,
dans la vitrine d'un bar, etc...

    https://envs.sh/tdz.zip 

|vign-affichette|

Téléchargement depuis ce site: `assange-affichettes.zip <../images/qr-code-blog-assange/assange-affichettes.zip>`_

Flyers
================================================================================


Les liens vers les fichiers imprimables des flyers
qui ont été distribués
au cours de la campagne.

Flyer renvoyant vers ce blog
------------------------------------------------------------------------
Il n'y a pas de date d'évènement dessus.
Tant que le site web reste en place,
ces flyers restent
valides.
Pour créer des flyers envoyant vers un autre blog,
il faudra changer le QR-code.

La manière de créer d'autres qr-codes
est précisée
dans la section qr-code-how-to_

La manière de réutiliser les fichiers source
de flyers est détaillé
à partir du repère reutiliser-flyers-affiches_

Les flyers sont téléchargeables à cet endroit:

    https://envs.sh/F-6.zip 

recto: |vign-flyer-recto|
verso: |vign-flyer-verso|
recto x09: |vign-flyer-x09-recto|
verso x09: |vign-flyer-x09-verso|

Téléchargement depuis ce site: `flyers-blog-assange.zip <../images/qr-code-blog-assange/flyers-blog-assange.zip>`_


Flyers pour des évènements passés
------------------------------------------------------------------------

Flyers (Stencils) du 
https://envs.sh/Fr0.zip

Flyers (Stencils) du 17 février 2024
https://envs.sh/Fkg.zip

Flyers Aquilenet originaux du 22 janvier 2024
https://cloud.aquilenet.fr/s/PH9PoR5s5BxFrTt

QR-codes
================================================================================



QRcode du site Web blog-assange-bdx.frama.io
------------------------------------------------------------------------


Téléchargement des Fichiers .pdf imprimables
de QRcodes à découper
(à rajouter sur des affiches, etc...)
sur ce lien:

    https://envs.sh/tdY.zip  

plusieurs versions: |vign-qrcode-2x3| 2x3, |vign-qrcode-3x4| 3x4, etc... 

Téléchargement depuis ce site: `qr-codes-a-decouper.zip <../images/qr-code-blog-assange/qr-codes-a-decouper.zip>`_

.. ~ .. figure:: ../images/qr-code-blog-assange/QRcode-blog-assange-bdx.png
.. ~    :align: right
.. ~    :scale: 150 %
.. ~    :alt: map to buried treasure

.. ~    `QRcode-blog-assange-bdx.png <../images/qr-code-blog-assange/QRcode-blog-assange-bdx.png>`_

.. ~ .. figure:: ../images/qr-code-blog-assange/QRcode-blog-assange-bdx.svg
.. ~    :scale: 100 %
.. ~    :alt: map to buried treasure

.. ~    `QRcode-blog-assange-bdx.svg <../images/qr-code-blog-assange/QRcode-blog-assange-bdx.svg>`_


------------------------------------------------------------------------

.. _reutiliser-flyers-affiches:

reutiliser-flyers-affiches_

Adapter les Flyers
===================

Pour retoucher les fichiers d'origine et faire
des flyers pour la mobilisation dans une autre ville,
il faudra:

* Avoir un website

    avec une autre url.
    Pour relayer les évènement locaux

* Créer un QR-code

* Remplacer url et qr-code

    Remplacer url et qr-code dans les fichiers source.
    De manière à avoir des fichiers imprimables
    adaptés à ce nouveau website.

* Vérifier le QR-code

    Imprimer des flyers, afin de vérifier
    que le QR-code fonctionne bien.
    Que ce soit du point de vue de la taille
    du QR-code,
    qui doit être suffisamment gros
    pour qu'un smartphone puisse le lire.

    Et vérifier qu'il renvoie bien
    vers la nouvelle url.



.. _qr-code-how-to:

Créer un QR-code
----------------

Les commandes qu'il a fallu utiliser sous linux
pour générer les fichiers image
.svg et .png
des QR-codes qui envoient
vers l'url de ce site sont les suivantes:

.. code-block:: sh

    qrencode -t SVG "https://blog-assange-bdx.frama.io" > QRcode-blog-assange-bdx.svg

    qrencode -t PNG -o QRcode-blog-assange-bdx.png "https://blog-assange-bdx.frama.io"

    zbarimg -q --raw QRcode-blog-assange-bdx.png

Pour adapter les Flyers à une autre ville que Bordeaux,
il faudra changer cette url pour celle
du blog de la ville correspondante.

Adapter les fichiers source
---------------------------

Adapter l'Affiche
~~~~~~~~~~~~~~~~~
Les fichiers source sont un fichier libre-office
dans le cas de l'affiche:
`affiche-wikileaks-qrcode.odg </images/adapter-flyers/affiche-wikileaks-qrcode.odg>`_ .
Il faut remplacer le QR-code.
Pour pouvoir faire d'autres affiches personnalisées,
il est aussi possible de partir du fichier jpeg de départ,
`assange-stencil.jpg  </images/adapter-flyers/assange-stencil.jpg>`_ .
Vectoriser ce fichier permettra peut être d'obtenir de meilleurs résultats.

Adapter le Flyer
~~~~~~~~~~~~~~~~
Le fichier source du verso du flyer est un fichier .xcf ,
le format de fichier utilisé par Gimp:
`flyer-QRcode-blog-IPNS.xcf </images/adapter-flyers/flyer-QRcode-blog-IPNS.xcf>`_
Il faut remplacer le QR-code et l'url.

Générer les .pdf
~~~~~~~~~~~~~~~~

Quand les nouveau fichiers sont satisfaisant,
il faut les exporter au format .pdf ,
imprimable sur une page.

Ensuite, il va falloir falloir faire un fichier
dans lequel la même page est répétée 16 fois.
Par exemple, dans le cas où l'on a un fichier
sur une page
assange-wikileaks-extradition-A4.pdf:

.. code-block:: shell

    pdftk assange-wikileaks-extradition-A4.pdf assange-wikileaks-extradition-A4.pdf assange-wikileaks-extradition-A4.pdf assange-wikileaks-extradition-A4.pdf cat output assange-wikileaks-extradition-A4-x4.pdf
    pdftk assange-wikileaks-extradition-A4-x4.pdf assange-wikileaks-extradition-A4-x4.pdf assange-wikileaks-extradition-A4-x4.pdf assange-wikileaks-extradition-A4-x4.pdf cat output assange-wikileaks-extradition-A4-x16.pdf
    
On obtient un fichier
assange-wikileaks-extradition-A4x16.pdf ,
dans lequel la même page a été répétée 16 fois.

C'est ce fichier qu'il faudra ensuite imprimer
vers un fichier .pdf final.
En 4 pages pour un A4, 9 pages pour un A4, etc...
en plusieurs copies,
de manière à avoir des flyers de la taille désirée
découpables dans un A4.

Attention à bien vérifier que l'impression
se fait en spécifiant "Marges: aucune"
pour que les proportions soient respectées.

À partir de là, les fichiers pdf finaux en 4xpage, 9xpage etc...
peuvent être publiés et utilisés.
