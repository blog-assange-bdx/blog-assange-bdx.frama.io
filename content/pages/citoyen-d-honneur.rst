Citoyen-d-Honneur
################################################################################

:page_order: 5500


.. note:: 
    **Attention!** : *Pour l'instant cette page est à peine commencée,
    elle est encore loin d'être à jour.*

Il y a eu plusieurs municipalités de Gironde
qui ont été démarchées,
pour leur demander d'accorder
le statut de Citoyen d'Honneur à Julian Assange.

Bordeaux Métropole
================================================================================

Bordeaux
----------

Une pétition circule
pour demander à l'équipe municipale de Bordeaux d'accorder le statut.
https://defcon.social/@freeassangewavebordeaux/112450792440997158
