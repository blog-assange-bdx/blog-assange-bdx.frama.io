Contacts
################################################################################

:page_order: 4000


Les canaux de communication publics qu'il sera possible
de suivre,
auxquels il sera possible de s'inscrire,
pour suivre l'actualité de la mobilisation en ce qui concerne Bordeaux.

Attention.
Il ne s'agit que des organisations de Bordeaux ou en Gironde.
Les organisations nationales ne figurent pas sur cette page,
elles peuvent être trouvées sur la page
`Participants <{filename}participants.rst>`_
à la
section
`Organisations Nationales <participants.html#orgas-nationales>`_

.. contents::  .
 :depth: 5
 
.. sectnum:: 

------------------------------------------------------------------------------


Chat: IRC / Xmpp / Matrix etc...
================================================================================

Les outils de chats en temps réel disponibles.
Plusieurs outils différents sont disponible,
qui utilisent plusieurs protocoles,
et demandent plus ou moins de connaissances techniques
pour être utilisés.
Il y a IRC, Xmpp, Matrix, qui utilisent des logiciels libres.
Il peut aussi éventuellement y avoir des outils
mais moins respectueux de la vie privée,
mais beaucoup plus simples d'utilisation
et dont l'usage est beaucoup plus répandu.

IRC: Internet_Relay_Chat
------------------------------------------------------------------------------
Un protocole assez ancien, qui peut aussi être utilisé
facilement
avec un navigateur web.

https://fr.wikipedia.org/wiki/Internet_Relay_Chat

chat IRC d'Aquilenet
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

ircs://#aquilenet@irc.libera.chat

Il est possible de participer aux discussions du chat irc 
d'Aquilenet
en utilisant l'interface web fournie par libera.chat:

    https://web.libera.chat/gamja/?join=#aquilenet

Il est aussi possible d'utiliser l'interface web fournie par le
site d'Aquilenet:

    http://tools.aquilenet.fr/irc.html

.. _simplex-chat:

SimpleX Chat
------------------------------------------------------------------------------

SimpleX Chat est un système de chat
beaucoup plus récent qu'IRC.
Son fonctionnement,
et les avantages qu'il va présenter
vis à vis de
Telegram, Signal et autres systèmes,
sont expliqués sur la page du projet:

    https://simplex.chat/

Son utilisation est assez simple,
par contre,
le fonctionnement interne,
destinée à donner un maximum d'anonymat
aux utilisateur,
par exemple en faisant
circuler les échanges au travers du
réseau Tor,
est assez élaboré.

C'est un système de messagerie
qui est notamment utilisé
par les berlinois,
pour les avantages
qu'il procure en terme
d'anonymisation
et de privacy,
vis à vis des systèmes traditionnels.

Et terme d'utilisation,
c'est assez simple,
il suffit de suivre un lien
vers un salon de discussion SimpleX Chat.

Si aucune application n'est installée
sur le système, le site web de SimpleX chat
réagira en renvoyant vers une page
qui détaille l'installation d'un client
SimpleX chat.

Il suffit donc de cliquer sur le lien
pour avoir les instructions pour
rejoindre le salon de discussion.


SimpleX Chat: #'The WikiLeaks Story'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
C'est un salon de discussion
qu'il est possible de rejoindre
pour échanger à propos de la
mobilisation en cours.

Attention, on sort du cadre local,
ce salon de discussion est internationnal,
avec des participants présents
dans toute l'Europe.

Il y a des interlocuteurs francophones,
mais aussi des possibilités d'échanger
en anglais, en espagnol, en italien,
en allemand, *etc...*

Il est possible de recevoir les instructions
pour rejoindre le salon simplement
en cliquant sur ce lien:

`https://simplex.chat/contact#/?v=1-4&smp=smp%3A%2F%2F1OwYGt-yqOfe2IyVHhxz3ohqo3aCCMjtB-8wn4X_aoY%3D%40smp11.simplex.im%2FlNstYq8btb4_R9k5WEzKuFidGNatfgV-%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAF7fVLXxjgtJJ-Zi5N125qhcqhFfuGXEhQVhlgmo_G0k%253D%26srv%3D6ioorbm6i3yxmuoezrhjk6f6qgkc4syabh7m3so74xunb5nzr4pwgfqd.onion&data=%7B%22type%22%3A%22group%22%2C%22groupLinkId%22%3A%22NTnpBpIwK-GF1iZ_5-amOw%3D%3D%22%7D
<https://simplex.chat/contact#/?v=1-4&smp=smp%3A%2F%2F1OwYGt-yqOfe2IyVHhxz3ohqo3aCCMjtB-8wn4X_aoY%3D%40smp11.simplex.im%2FlNstYq8btb4_R9k5WEzKuFidGNatfgV-%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAF7fVLXxjgtJJ-Zi5N125qhcqhFfuGXEhQVhlgmo_G0k%253D%26srv%3D6ioorbm6i3yxmuoezrhjk6f6qgkc4syabh7m3so74xunb5nzr4pwgfqd.onion&data=%7B%22type%22%3A%22group%22%2C%22groupLinkId%22%3A%22NTnpBpIwK-GF1iZ_5-amOw%3D%3D%22%7D>`_


Toute la procédure d'installation du client
et les commandes pour rejoindre le salon seront
données depuis le site de SimpleX chat,
qui est il faut le reconnaître
franchement bien fait.

Dans le cas d'un smartphone,
il suffira de flasher un QR-code.

Fils d'Information:
================================================================================

Il est possible de recevoir des informations
en recevant directement
des email,
en s'abonnant à des listes de diffusions
et des newsletters.
Il est également possible
de recevoir les informations en utilisant
un Newsreader
(un logiciel de lecture de flux .rss),
en s'abonnant à des flux .rss 

La diffusion des informations
n'étant pas homogène,
il convient de suivre aussi les publications
des réseaux sociaux
de chacune des organisations participantes.
Il n'y a
en effet
pour l'instant aucun flux
qui rassemble l'ensemble
de manière instantannée.

Listes de Diffusion par Mail publiques et Newsletters
------------------------------------------------------------------------------

Il est possible de s'inscrire à ces listes de discussion
publiques en remplissant un formulaire.
Pour cela il suffit de cliquer
sur le lien
vers la page web qui est donnée pour chacune des listes.

bistro@listes.aquilenet.fr
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
d'Aquilenet:

    https://listes.aquilenet.fr/subscribe/bistro

blahblah@listes.abul.org
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
de l'Abul:

    https://listes.abul.org/subscribe/blahblah

blog-assange-bdx@framagroupes.org
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
de ce blog:

    https://framagroupes.org/sympa/info/blog-assange-bdx

Newsreader: Feed .rss .atom
------------------------------------------------------------------------------
Pour être tenu au courant des nouvelles publications
de divers sites webs,
il est possible de s'abonner
à des flux .rss ou .atom .
En utilisant un lecteur de flux .rss
(Outlook, Liferea, Thunderbird, etc...)

Les liens pour suivre les publications des sites suivants
peuvent être utilisés
par un lecteur de flux .rss :

.rss Free Assange Wave Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Il est possible d'être averti des nouvelles publications
de la page mastodon de Free Assange Wave Bordeaux
en s'abonnant à ce flux .rss:

    https://defcon.social/@freeassangewavebordeaux.rss



.rss Blog Assange Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Il est possible d'être averti des nouvelles publications
du blog blog-assange-bdx.frama.io
en s'abonnant soit à un flux .rss,
soit à un flux .atom:

https://blog-assange-bdx.frama.io/atom.xml

https://blog-assange-bdx.frama.io/rss.xml

.. _rss-modifications:

.rss modifications de Blog Assange Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Les deux flux .rss ci dessus n'envoient une notification
que lorsque un nouvel article est publié.
Mais ne permettent pas de savoir quand est ce que du contenu
a été ajouté ou modifié.

L'abonnement au flux .rss suivant:

    https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io.atom

`blog-assange-bdx.frama.io activity
<https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io.atom>`_
,
permet d'être notifié à chaque fois que le site web est modifié,
lorsque du contenu est ajouté.

Attention,
ces notifications sont beaucoup plus fréquentes.
Les mises à jours peuvent être faites
aussi bien pour mettre à jour le contenu d'un article,
que lorsque des informations sont ajoutées sur une page,
ou que simplement des fautes d'orthographes sont corrigées.

Dans chacune des notifications envoyées,
il y a un lien de la forme:

    https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io/-/compare/437724157d182c0fbcdc0670ea41861c8dc9d341...5388f5dd137a

Qui envoient vers une page web du site framagit.org,
qui héberge le site blog-assange-bdx.frama.io.
En bas de cette page,
cette interface web montre
quelles sont les lignes
qui ont été
ajoutées/modifiées/supprimées ,
les fichiers
ajoutés/modifiés/supprimés
lors de cette mise à jour du site web.

Enfin,
l'historique complet
des modifications
est accessibles
sur cette page
"relativement lisible":

    https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io/-/commits/5388f5dd137a/?ref_type=undefined


Réseaux Sociaux
================================================================================

Mastodon
------------------------------------------------------------------------------

Mastodon est une alternative à Twitter et Facebook

Mastodon de Free Assange Wave Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page du profil mastodon de Free Assange Wave Bordeaux
relaye en direct l'actualité de cette mobilisation:

    https://defcon.social/@freeassangewavebordeaux

Il est possible de s'abonner au flux .rss pour être notifié
des nouvelles publications du profil.

Twitter
------------------------------------------------------------------------------


Facebook
------------------------------------------------------------------------------

Ligue des Droits de l'Homme Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Les actualités de la section bordelaise de la LDH
sont annoncées sur leur profil Facebook,
sur cette page:

    https://www.facebook.com/LDHBordeauxGironde/


Sites Web Traditionnels
================================================================================

En dehors des pages de profils des réseaux sociaux,
il est aussi possible de se rendre sur le site web
des organisations qui n'utilisent pas de réseaux sociaux,
mais utilise le site web principal,
pour afficher leurs actualités.

Organisations participantes
------------------------------------------------------------------------

Amnesty bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
En se qui concerne Amnesty International,
pour suivre les publications de la section bordelaise
nous n'avons trouvé que leur site web.

    https://www.amnesty.fr/pres-de-chez-vous/bordeaux-sud

Ceci dit, leur site donne une adresse mail de contact,
bordeauxsud@amnestyfrance.fr ,
et surtout une page instagram,
où il sera en effet possible
de suivre leurs actualités:

    https://www.instagram.com/amnesty_bordeaux_33/?hl=fr 
