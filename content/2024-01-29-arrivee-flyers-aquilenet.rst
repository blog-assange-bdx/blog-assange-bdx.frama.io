[20/01/2024] [Bordeaux | Mezzanine] Arrivée des Flyers au Local Aquilenet
################################################################################

:date: 2024-01-29  12:26
:category: Flyers-Affiches-etc

L'arrivée des Flyers imprimés en couleur au Local Aquilenet,
annoncée sur la liste de diffusion bistro@aquilenet.fr.
À aller rechercher là bas au local de la Mezzanine.

Ce sont les flyers dont les fichiers imprimables avaient été publiés
à cette adresse:

    https://cloud.aquilenet.fr/s/PH9PoR5s5BxFrTt
