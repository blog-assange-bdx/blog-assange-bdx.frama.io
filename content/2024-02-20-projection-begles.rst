[20/02/2024] [Bègles | Théatre du Levain] Projection de "Ithaka"
################################################################################

:date: 2024-02-20 20:00
:category: Projections

Théâtre le Levain, 26 rue de la République, Bègles

Mardi 20 Février à 20 h à la projection du film documentaire

Entrée à prix libre ; la billetterie sera intégralement reversée au 
soutien juridique de Julian Assange.

ITHAKA, le combat pour libérer Julian ASSANGE

réalisé par Ben Lawrence, le père de Julian Assange et distribué en 
France par

Les Mutins de Pangée 
https://www.lesmutins.org/ithaka-le-nouveau-film-pour-la
