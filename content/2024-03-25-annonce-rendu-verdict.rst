[26/03/2024] [Londres] Rendu du Verdict
################################################################################

:date: 2024-02-25 18:19
:category: Rassemblements

Le Verdict du Procès est annoncé pour le 26 mars à 10h30
(heure de Londres, 11h30 en France) au *Royal Courts of Justice*.

    https://defcon.social/@freeassangewavebordeaux/112157468733259790

En réponse, un Rassemblement au Miroir d'eau est prévu,
on ne sait pas encore si ce rassemblement sera le jour même ou samedi.

Les infos complémentaires arriveront sur cette page:

    https://defcon.social/@freeassangewavebordeaux
