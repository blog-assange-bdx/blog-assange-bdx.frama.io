[18/05/2024] [Bordeaux | Victoire] Village des Libertés
################################################################################

:date: 2024-05-18 15:19
:category: Rassemblements

Cette année,
La Ligue des Droits de l'Homme
organise son congrès national à Bordeaux.
La municipalité annonce les activités ici:
     
    https://www.bordeaux.fr/e218972/congres-de-la-ligue-des-droits-de-l-homme-ldh-

.. note::
    **EDIT!!!:** *Une Chaîne Humaine est prévue à 12h00,
    en plus de la présence des organisateurs toute la journée.*
    https://defcon.social/@freeassangewavebordeaux/112438791249761011

Et donc, le samedi 18/05, place de la Victoire à Bordeaux,
pendant la journée,
à partir de 10h00,
jusqu'à 18h00,
se tiendra le Village des Libertés.
Où de nombreuses organisations participantes tiendront un stand.

Les organisations participantes à la campagne de mobilisation
pour la défense de Julian Assange seront représentées.
Ce sera donc l'occasion de les rencontrer IRL
et de pouvoir échanger sur ce sujet.

Il y aura Aquilenet:    https://www.aquilenet.fr/actualit%C3%A9s/congr%C3%A8s-de-ligue-des-droits-de-lhomme-et-village-associatif/

Il y aura FreeAssangeWave-Bordeaux au stand des médias:
https://defcon.social/@freeassangewavebordeaux/

Il y aura aussi Amnesty International (sur cette annonce, on peut voir qu'ils
sont ouverts aux candidatures bénévoles pour participer à l'animation du stand):
https://www.tousbenevoles.org/trouver-une-mission-benevole/operation-de-sensibilisation/33/bordeaux/sensibiliser-a-la-defense-des-droits-humains-lors-du-village-des-droits-et-libertes-70358

Et évidement la LDH de Bordeaux.


.. image:: ../images/village_associatif-600x0.png
                         :target: https://www.aquilenet.fr/actualit%C3%A9s/congr%C3%A8s-de-ligue-des-droits-de-lhomme-et-village-associatif/
                         :alt: affiche village des libertés

Sur le site d'Aquilenet, sur celui de la ville, et ailleurs, il sera possible de trouver plus d'information,
sur cette manifestation, et sur les autres manifestations publiques
organisées ce WE
(exposition à la Fac de Bordeaux située -elle aussi-
place de la Victoire,
la rencontre vendredi soir à la Halle des Douves,
etc...)  
