[15/02/2024] [Carbon Blanc | Cinéma] Projection de "The War on Journalism"
################################################################################

:date: 2024-02-15  20:00
:category: Projections


Projection-débat gratuite du documentaire The War On Journalism (Juan Passarelli / 2020) est organisée le jeudi 15 février à 20h au cinéma de Carbon Blanc.

https://lagrappe.info/?Projection-debat-War-on-Journalism-Liberte-pour-Julian-Assange-637
