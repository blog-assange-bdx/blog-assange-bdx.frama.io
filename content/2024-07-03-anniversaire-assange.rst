[03/07/2024] Anniversaire Assange
################################################################################

La Comité Assange a publié un communiqué
donnant des détails à propos de
la libération d'Assange.

Il a aussi été évoqué ensuite,
dans leur newsletter du 27/06,
que ce soit le 3 juillet,
jour de l'anniversaire d'Assange,
que va être fêtée sa libération.

Le message est retranscrit à la fin de ce billet.

Il semble que pour l'instant
rien ne soit définitivement fixé.
Si ce sera une vague, un rassemblement
ou autre chose

Plus d'informations sont à venir
sur le site du Comité Assange:

    https://comiteassange.fr/actualites-et-communiques/

Et localement sur leur antenne à Bordeaux:

    https://defcon.social/@freeassangewavebordeaux


L'annonce originale sur la newsletter du Comité Assange::

    ----- Message Transféré -----

    Date : Thu, 27 Jun 2024 11:55:11 +0000
    De : Comité de soutien Assange <comitesoutienassange@protonmail.com>
    À : undisclosed-recipients:;
    Sujet : Julian Assange enfin libre ! – le podcast des Mutins, avec
    Laurent Dauré et Viktor Dedaj


    Bonjour,

    Voici le récit à chaud d’une victoire, celle d’une lutte acharnée pour
    le droit d’informer et la liberté, avec Laurent Dauré et Viktor Dedaj.
    Un podcast des Mutins de Pangée, par Olivier Azam :

    https://www.lesmutins.org/julian-assange-enfin-libre-le

    Les Mutins de Pangée ont été des camarades exemplaires dans cette
    lutte, en informant sans relâche, en distribuant en France et en
    accompagnant deux documentaires sur l’affaire Assange : Hacking
    JusticeetIthaka. Grâce à des centaines de projections-débats, la
    connaissance et la mobilisation ont grandi, favorisant le développement
    du mouvement de solidarité.

    Nous allons organiser un événement pour fêter la libération de Julian
    Assange. Ce sera probablement le 3 juillet (mercredi prochain), le jour
    de son anniversaire. Nous vous tiendrons évidemment au courant.

    À très vite,

    Comité de soutien Assange[comiteassange.fr](https://comiteassange.fr/)

    Envoyé avec la messagerie sécurisée [Proton Mail.](https://proton.me/)
