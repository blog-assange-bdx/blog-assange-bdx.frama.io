[01/02/2024] [Bordeaux | Halle des Douves] Projection "The War on journalism"
################################################################################

:date: 2024-02-01  19:00
:category: Projections

Projection à la Halle des Douves à Bordeaux

Entrée Libre

The War on journalism (J.Passarelli 2020)

https://www.douves.org/agenda/
