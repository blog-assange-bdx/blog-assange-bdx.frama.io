[21/05/2024] [Bordeaux] Rassemblement national
################################################################################

:date: 2024-05-21 18:00
:category: Rassemblements

**Edit 21/05 à 11h45** il n'y a pas de point de rendez vous officiel connu pour Bordeaux.

Bonjour,

Dans sa lettre d'information du 10 mai,
le Comité Assange a annoncé qu'il prévoit
un rassemblement national,
dans toutes les villes participant à la mobilisation,
le mardi 21 mai à 18h00.

Pour l'instant, le lieu du rassemblement à Bordeaux
n'a pas encore été déterminé. Ce pourra être à nouveau
au Miroir d'Eau, ou devant le Consulat des U.S.A.
https://fr.usembassy.gov/fr/u-s-consulate-bordeaux/
ou ailleurs.

L'information sera diffusée au plus vite dès sa publication.

Le site du Comité Assange tient à jour un lien
sur lequel figure la liste des villes participantes:

    https://comiteassange.fr/journee-de-mobilisation-pour-julian-assange-le-mardi-21-mai-en-france/

Mais il est donc déjà possible d'afficher et tracter pour annoncer
ce rassemblement, et d'autres manifestations en rapport
avec cette campagne.
