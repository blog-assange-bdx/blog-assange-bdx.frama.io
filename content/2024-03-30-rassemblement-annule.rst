[30/03/2024] [Bordeaux] Rassemblement reporté
################################################################################

:date: 2024-03-29 15:19
:category: Rassemblements

Le Rassemblement qui avait été évoqué pour le Samedi 30 mars au Miroir d'Eau
est annulé. Les informations complémentaires sont dans ce billet:

    https://defcon.social/@freeassangewavebordeaux/112175674223502821

Il avait été évoqué dans un précédent billet:

    https://defcon.social/@freeassangewavebordeaux/112157468733259790

Et donc, un rassemblement aura bien lieu, mais la date n'a pas encore été fixée.
