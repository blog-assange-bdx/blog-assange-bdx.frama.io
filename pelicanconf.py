AUTHOR = 'blog-assange-bdx'
SITENAME = 'blog-assange-bdx'

SITEURL = "https://blog-assange-bdx.frama.io"

PATH = "content"

OUTPUT_PATH = "public/"

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'fr'

THEME = 'theme/notmyidea-cms-fr'

PLUGIN_PATHS = ["plugins"]
PLUGINS = ['pin_to_top']

PAGE_ORDER_BY = 'page_order'

# Feed generation is usually not desired when developing
FEED_ATOM = u'atom.xml'
FEED_RSS = u'rss.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Free-Assange-Wave-Bdx", "https://defcon.social/@freeassangewavebordeaux"),
    ("Amnesty-Bdx", "https://www.amnesty.fr/pres-de-chez-vous/bordeaux-sud"),
    ("LDH-Bordeaux", "https://www.facebook.com/LDHBordeauxGironde/"),
    ("Aquilenet", "https://www.aquilenet.fr/actualit%C3%A9s/libert%C3%A9-pour-julian-assange/"),
    ("Amis du Diplo 33", "https://www.amis.monde-diplomatique.fr/-Gironde-.html"),
    ("Radio la Clé des Ondes", "https://www.lacledesondes.fr/"),
)

# ~ # Social widget
SOCIAL = (
    ("""Notification
    des modifs de ce blog blog-assange-bdx.frama.io""", "contacts#rss-modifications"),
    ("Démosphère-Gironde", "https://gironde.demosphere.net/search?search=Assange"),
    ("LaGrappe.info", "https://lagrappe.info/spip.php?page=recherche&recherche=Assange"),
    ("IRC #aquilenet@libera.chat", "https://web.libera.chat/gamja/?join=\#aquilenet"),
    ("SimpleX Chat: #'The WikiLeaks Story'", "contacts#simplex-chat"),
    ("Mailing List: \n [blog-assange-bdx @framagroupes.org]", "https://framagroupes.org/sympa/info/blog-assange-bdx" ),
    # ~ ("Framagit", "https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io"),

    # ~ ("Another social link", "#"),
)

DEFAULT_PAGINATION = 40

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
